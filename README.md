# README #

### What is this repository for? ###

This app is for computing the open terrain at ski resorts, and the cost per hour and acre for cost effectiveness.  It is built on top of jQuery mobile.  It can run as a stand-alone app, without a webserver.

![preview](http://i.imgur.com/vw5ADoz.jpg)

### How do I get set up? ###

* Summary of set up
    * Download the repo and run default.html
* Configuration
    * None, unless you want to add more data to the data.js file
* Dependencies
    * None
* Database configuration
    * All data is in data.js
* How to run tests
    * n/a
* Deployment instructions
    * Save and Run

### Contribution guidelines ###

* Writing tests
    * n/a
* Code review
    * n/a
* Other guidelines
    * Contribute whatever you would like.
    * Resort acreage is figured out by drawing polygons to measure in Google Earth.  If you add more resorts, please add the data to data.js and upload the KMZ file to the "google earth files" folder for others to use.

### Who do I talk to? ###

* Repo owner or admin
    * ideapointcreative